# DODE

Dode is a Dockerized nvm-like node helper.

Based off [nvm](https://github.com/nvm-sh/nvm).

## Why

To keep the system clean. A developer's computer shouldn't be filled with development tools, only content.

## Requirements

- Docker

## Usage

To specify a node version to use, you may create a `.nvmrc` file or run the command with the environment variable `DODE_VERSION`.

To specify a docker variant, you may add a second line to `.nvmrc` with the variant or run the command with the environment variable `DODE_VARIANT`. By default will use `slim` variants.

```bash
# Default, using latest version
dode.sh <your node/npm/yarn command>

# Using a .nvmrc file
echo "8.6" > .nvmrc
dode.sh <your node/npm/yarn command>

# Using an environment variable
DODE_VERSION=8.6 dode.sh <your node/npm/yarn command>

# Using a different variant
DODE_VARIANT=alpine dode.sh <your node/npm/yarn command>

# Using a different variant via .nvmrc
echo "stretch" >> .nvmrc
dode.sh <your node/npm/yarn command>
```

By default it exposes the port `3000`. You can change this behavior by specifying the `DODE_PORT` environment variable.

```bash
PORT=3001 dode.sh node src/index.js
```

To simplify usage, you can add the following aliases to your `.bashrc`, `.zshrc`...:

```sh
alias dode="dode.sh node"
alias dpm="dode.sh npm"
alias darn="dode.sh yarn
```

### Available options

| Option | Default | Description |
|--------|---------|-------------|
| DODE_VERSION | latest | Node version to use |
| DODE_VARIANT | slim | Docker image variant to use, when not using the `latest` DODE_VERSION |
| DODE_PORT | 3000 | Port to expose |

### .nvmrc

Dode will try to read the contents of the nearest `.nvmrc` file available.

The first line must contain the node version to use (as a regular nvm file).

A second (optional) line may exist to specify the docker image variant to use.

```.nvmrc
8
stretch
```

### .npmrc

By default will map your user's `$HOME/.npmrc`, so you can share your configuration with every project.
