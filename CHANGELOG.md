<!-- markdownlint-disable MD024 -->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.2.0] - 2019-08-29

### Added

- Hability to specify the `DODE_VARIANT` value with the `.nvmrc` file

### Changed

- Updated README

## [v1.1.0] - 2019-08-29

### Added

- Added support to specify docker image variants with `DODE_VARIANT`

### Changed

- Renamed `PORT` to `DODE_PORT`
- Renamed `NODE_VERSION` to `DODE_VERSION`
- Updated README

## [v1.0.0] - 2019-08-29

### Added

- Initial implementation

<!-- This section adds links to the version headers in order to review changes between versions -->

[unreleased]: https://gitlab.com/gerardrubio/dode/compare/v1.2.0...master
[v1.2.0]: https://gitlab.com/gerardrubio/dode/compare/v1.1.0...v1.2.0
[v1.1.0]: https://gitlab.com/gerardrubio/dode/compare/v1.0.0...v1.1.0
[v1.0.0]:https://gitlab.com/gerardrubio/dode/tree/v1.0.0
