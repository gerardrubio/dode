#!/bin/sh

# Helper methods extracted from https://github.com/nvm-sh/nvm/blob/master/nvm.sh

nvm_echo() {
  command printf %s\\n "$*" 2>/dev/null
}

nvm_err() {
  >&2 nvm_echo "$@"
}

# Traverse up in directory tree to find containing folder
nvm_find_up() {
  local path_
  path_="${PWD}"
  while [ "${path_}" != "" ] && [ ! -f "${path_}/${1-}" ]; do
    path_=${path_%/*}
    nvm_echo "${path_}"
  done
  nvm_echo "${path_}"
}

nvm_find_nvmrc() {
  if [ -e "${PWD}/.nvmrc" ]; then
    nvm_echo "${PWD}/.nvmrc"
  else
    local dir
    dir="$(nvm_find_up '.nvmrc')"
    if [ -e "${dir}/.nvmrc" ]; then
        nvm_echo "${dir}/.nvmrc"
    fi
  fi
}

# Obtain nvm version from rc file
nvm_rc_version() {
  export NVM_RC_VERSION='latest'
  local NVMRC_PATH
  NVMRC_PATH="$(nvm_find_nvmrc)"
  if [ ! -e "${NVMRC_PATH}" ]; then
    nvm_err "No .nvmrc file found, defaulting version to latest"
    return 1
  fi
  NVM_RC_VERSION="$(command head -n 1 "${NVMRC_PATH}" | command tr -d '\r')" || command printf ''
  if [ -z "${NVM_RC_VERSION}" ]; then
    nvm_err "Warning: empty .nvmrc file found at \"${NVMRC_PATH}\""
    return 2
  fi
  nvm_echo "Found '${NVMRC_PATH}' with version <${NVM_RC_VERSION}>"
}

# Obtain variant from rc file
nvm_rc_variant() {
  export DOCKER_VARIANT='slim'
  local NVMRC_PATH
  NVMRC_PATH="$(nvm_find_nvmrc)"
  if [ ! -e "${NVMRC_PATH}" ]; then
    nvm_err "No .nvmrc file found, defaulting variant to slim"
    return 1
  fi
  DOCKER_VARIANT="$(command sed '2q;d' "${NVMRC_PATH}" | command tr -d '\r')" || command printf ''
  if [ -z "$DOCKER_VARIANT" ]; then
    nvm_err "No variant declared in .nvmrc file, defaulting variant to slim"
    DOCKER_VARIANT='slim'
    return 1
  fi
  nvm_echo "Found '${NVMRC_PATH}' with variant <${DOCKER_VARIANT}>"
}

NVM_RC_VERSION="$DODE_VERSION"
if [ -z "$NVM_RC_VERSION" ]; then
    nvm_rc_version
fi

DOCKER_VARIANT="$DODE_VARIANT"
if [ -z "$DOCKER_VARIANT" ]; then
    nvm_rc_variant
fi

if [ "$NVM_RC_VERSION" != "latest" ]; then
    NVM_RC_VERSION="$NVM_RC_VERSION-$DOCKER_VARIANT"
fi

PORT="$DODE_PORT"
if [ -z "$PORT" ]; then
    PORT=3000
fi

WORKING_DIR=/app

docker run \
    --rm \
    -v $HOME/.npmrc:/$WORKING_DIR/.npmrc \
    -v $PWD:$WORKING_DIR \
    -w $WORKING_DIR \
    -u node \
    -p $PORT:$PORT \
    -it node:$NVM_RC_VERSION \
    $@
